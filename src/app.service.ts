import { HttpException, Injectable } from '@nestjs/common';
import { ConfigService } from 'nestjs-dotenv';
import { HttpService } from '@nestjs/axios';
import { catchError, map, lastValueFrom, of } from 'rxjs';
import {
  MyResponse,
  Pages,
  ReqCity,
  ResAPIExchangeFree,
  ResAPIWeather,
  ResAPIWeatherCoord,
  ResAPIWikipedia,
} from './model.types';
import { getAllInfoByISO } from 'iso-country-currency';
import * as dayjs from 'dayjs';

@Injectable()
export class AppService {
  constructor(
    private readonly httpService: HttpService,
    private configService: ConfigService,
  ) {}

  async getWeather(req: ReqCity): Promise<MyResponse> {
    const request$ = this.httpService
      .get(
        `https://api.openweathermap.org/data/2.5/weather?lat=${req.lat}&lon=${
          req.lon
        }&appid=${this.configService.get('e0c7d4f115292a41b68c9d91c58d9033')}`,
      )
      .pipe(
        map((axiosResponse: any) => {
          return axiosResponse;
        }),
        catchError((e) => {
          return of(e.response);
        }),
      );

    const res: any = await lastValueFrom(request$);

    if (res.status != 200) {
      console.log('Error: ', res);
      throw new HttpException(res.data, res.status);
    }

    const data: ResAPIWeather = res.data;

    const reqWikipedia$ = this.httpService
      .get(
        `https://es.wikipedia.org/w/api.php?action=query&prop=extracts&titles=${req.city}&exintro=&exsentences=2&explaintext=&redirects=&format=json`,
      )
      .pipe(
        map((axiosResponse: any) => {
          return axiosResponse;
        }),
        catchError((e) => {
          return of(e.response);
        }),
      );
    const resWikipedia: any = await lastValueFrom(reqWikipedia$);

    if (resWikipedia.status != 200) {
      console.log('Error: ', resWikipedia);
      throw new HttpException(resWikipedia.data, resWikipedia.status);
    }

    const dataWiki: ResAPIWikipedia = resWikipedia.data;

    const arr: Pages[] = Object.values(dataWiki.query.pages).filter(
      (el) => el !== null,
    );

    let description = '';
    if (arr.length > 0) description = arr[0].extract;

    const datetime: string = dayjs.unix(data.dt).toISOString();

    const currency: string = getAllInfoByISO(data.sys.country).currency;

    const reqExchange$ = this.httpService
      .get(
        `https://v6.exchangerate-api.com/v6/f26aaf4529f296c77fdad4ff/latest/USD`,
      )
      .pipe(
        map((axiosResponse: any) => {
          return axiosResponse;
        }),
        catchError((e) => {
          return of(e.response);
        }),
      );

    const resExchange: any = await lastValueFrom(reqExchange$);

    if (res.status != 200) {
      console.log('Error: ', res);
      throw new HttpException(res.data, res.status);
    }

    const dataExchange: ResAPIExchangeFree = resExchange.data;

    const arrExchange: string[] = Object.keys(
      dataExchange.conversion_rates,
    ).filter((el) => el === currency);

    const lastResponse: MyResponse = {
      title: req.city,
      description: description,
      country: data.sys.country,
      lat: req.lat,
      lng: req.lon,
      currentISO: currency,
      perDolar: dataExchange.conversion_rates[arrExchange[0]],
      date: datetime, //timezone
      weather: {
        title: data.weather[0].main,
        icon: data.weather[0].icon,
        humidity: data.main.humidity,
        temperature: data.main.temp - 273.0,
      },
    };

    return lastResponse;
  }

  async getCities(city: string): Promise<ResAPIWeatherCoord> {
    const request$ = this.httpService
      .get(
        `http://api.openweathermap.org/geo/1.0/direct?q=${city}&limit=5&appid=699e217dd1fd1081e48219769d2d735a`,
      )
      .pipe(
        map((axiosResponse: any) => {
          return axiosResponse;
        }),
        catchError((e) => {
          return of(e.response);
        }),
      );

    const res: any = await lastValueFrom(request$);
    if (res.status != 200) {
      console.log('Error: ', res);
      throw new HttpException(res.data, res.status);
    } else {
      const data: ResAPIWeatherCoord = res.data;
      return data;
    }
  }
}
