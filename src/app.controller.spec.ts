import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppModule } from './app.module';
import { AppService } from './app.service';
import { ResAPIWeatherCoord } from './model.types';


describe('AppController', () => {
  let appController: AppController;
  let appService: AppService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [AppModule]
    }).compile();

    appController = app.get<AppController>(AppController);
    appService = app.get<AppService>(AppService);
  });

  it('Service | should create method getCities', () => {
    expect(appService.getCities).toBeDefined();
  })

  it('Service | should create method getCities', () => {
    expect(appService.getWeather).toBeDefined();
  })


  describe('getCities', () => {
    it('should return Buenos Aires', async () => {
      const result = [{
        name : "Autonomous City of Buenos Aires",
        local_names: {
            "ur": "بیونس آئرس",
            "lt": "Buenos Airės",
            "sr": "Буенос Ајрес",
            "mk": "Буенос Аирес",
            "bg": "Буенос Айрес",
            "my": "ဗျူနိုအေးရိစ်မြို့",
            "is": "Búenos Aíres",
            "be": "Буэнас-Айрэс",
            "hu": "Buenos Aires",
            "he": "בואנוס איירס",
            "am": "ብዌኖስ አይሬስ",
            "fr": "Buenos Aires",
            "ru": "Буэнос-Айрес",
            "bo": "པུ་ཨེ་ནོ་སི་ཨས་རི་སི།",
            "oc": "Buenos Aires",
            "hy": "Բուենոս Այրես",
            "uk": "Буенос-Айрес",
            "uz": "Buenos Ayres",
            "yi": "בוענאס איירעס",
            "th": "บัวโนสไอเรส",
            "ta": "புவெனஸ் ஐரிஸ்",
            "ar": "بوينس آيرس",
            "en": "Autonomous City of Buenos Aires",
            "ba": "Буэнос-Айрес",
            "ku": "Buenos Aires",
            "fa": "بوئنوس آیرس",
            "la": "Bonaëropolis",
            "lv": "Buenosairesa",
            "bn": "বুয়েনোস আইরেস",
            "ht": "Bwènozè",
            "az": "Buenos Ayres",
            "zh": "布宜诺斯艾利斯",
            "ml": "ബ്യൂണസ് ഐറീസ്",
            "os": "Буэнос-Айрес",
            "ko": "부에노스아이레스",
            "ka": "ბუენოს-აირესი",
            "gl": "Bos Aires",
            "ky": "Буэнос-Айрес",
            "pl": "Buenos Aires",
            "eo": "Bonaero",
            "et": "Buenos Aires",
            "es": "Buenos Aires",
            "ab": "Буенос-Аирес",
            "ja": "ブエノスアイレス",
            "el": "Μπουένος Άιρες",
            "ce": "Буэнос-Айрес",
            "tl": "Lungsod ng Buenos Aires",
            "de": "Buenos Aires",
            "tk": "Buenos-Aýres",
            "hi": "ब्यूनस आयर्स",
            "ug": "Buénos Ayrés",
            "kn": "ಬ್ವೇನೊಸ್ ಐರೇಸ್",
            "tg": "Буэнос Айрес",
            "sv": "Buenos Aires",
            "mn": "Буэнос-Айрес",
            "mr": "बुएनोस आइरेस"
        },
        lat: -34.6075682,
        lon:  -58.4370894,
        country: "AR",
        state: "Autonomous City of Buenos Aires"
      }] as ResAPIWeatherCoord
      expect(await appController.getCities({city: 'Buenos Aires, Autonomous City of Buenos Aires, AR'})).toStrictEqual(result);
    });
  });
});
