import { ApiProperty } from '@nestjs/swagger';

export class ReqCity {
  @ApiProperty()
  lat: string;
  @ApiProperty()
  lon: string;
  @ApiProperty()
  city: string;
}

export class ParamCity {
  @ApiProperty()
  city: string;
}

export type Coordinates = {
  lat: string;
  lon: string;
};

export type ResAPIWeather = {
  coord: Coordinates;
  weather: [
    {
      id: number;
      main: string;
      description: string;
      icon: string;
    },
  ];
  base: string;
  main: {
    temp: number;
    feels_like: number;
    temp_min: number;
    temp_max: number;
    pressure: number;
    humidity: number;
  };
  visibility: number;
  wind: {
    speed: number;
    deg: number;
  };
  clouds: {
    all: number;
  };
  dt: number;
  sys: {
    type: number;
    id: number;
    country: string;
    sunrise: number;
    sunset: number;
  };
  timezone: number;
  id: number;
  name: string;
  cod: number;
};

export type ResAPIWeatherCoord = [
  | {
      name: string;
      local_names: {
        [key: string]: string;
      };
      lat: number;
      lon: number;
      country: string;
      state: string;
    }
  | {
      name: string;
      lat: number;
      lon: number;
      country: string;
      state: string;
    },
];

export type ResAPIWikipedia = {
  batchcomplete: '';
  query: {
    normalized: [
      {
        from: string;
        to: string;
      },
    ];
    redirects: [
      {
        from: string;
        to: string;
      },
    ];
    pages: {
      [key: string]: Pages;
    };
  };
};

export type Pages = {
  pageid: number;
  ns: number;
  title: string;
  extract: string;
};

export type MyResponse = {
  title: string;
  description: string;
  country: string;
  lat: string;
  lng: string;
  currentISO: string;
  perDolar: number;
  date: string;
  weather: Weather;
};

export type Weather = {
  title: string;
  icon: string;
  humidity: number;
  temperature: number;
};

export type ResAPIExchange = {
  result: string;
  documentation: string;
  terms_of_use: string;
  time_last_update_unix: number;
  time_last_update_utc: string;
  time_next_update_unix: number;
  time_next_update_utc: string;
  base_code: string;
  target_code: string;
  conversion_rate: number;
};

export type ResAPIExchangeFree = {
  result: string;
  documentation: string;
  terms_of_use: string;
  time_last_update_unix: number;
  time_last_update_utc: string;
  time_next_update_unix: number;
  time_next_update_utc: string;
  base_code: string;
  conversion_rates: {
    [key: string]: number;
  };
};
