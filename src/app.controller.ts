import {
  Body,
  Controller,
  Post,
  HttpException,
  Param,
  Get,
} from '@nestjs/common';
import { AppService } from './app.service';
import {
  MyResponse,
  ParamCity,
  ReqCity,
  ResAPIWeatherCoord,
} from './model.types';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('weather')
  async sendWeather(@Body() request: ReqCity): Promise<MyResponse> {
    try {
      return await this.appService.getWeather(request);
    } catch (error) {
      throw new HttpException(`Error: , ${error}]`, 400);
    }
  }

  @Get('cities/:city')
  async getCities(@Param() param: ParamCity): Promise<ResAPIWeatherCoord> {
    try {
      return await this.appService.getCities(param.city);
    } catch (error) {
      throw new HttpException(`Error: , ${error}]`, 400);
    }
  }
}
